/** @type {import('tailwindcss').Config} */
module.exports = {
  // watch components, layouts, pages, plugins, nuxt.config.js for hot reload
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue"
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

