// https://nuxt.com/docs/api/configuration/nuxt-config
import { resolve} from 'path';
import { fileURLToPath } from 'url';

export default defineNuxtConfig({
  alias: {
    // '@': resolve(__dirname, "/"), // root folder
    "assets": "./assets",
    "public": "./public",
    'data': fileURLToPath(new URL('./assets/other/data', import.meta.url))
  },
  hooks: {
    ready: (ctx) => console.log(ctx) // hook will run when f5
  },
  css: [
    '@/assets/main.css',
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },
  modules: [
    '@nuxt/content',
    '@nuxtjs/i18n',
    '@pinia/nuxt'
  ],
  i18n: {
    strategy: 'prefix_except_default',
    langDir: './lang',
    defaultLocale: 'ja',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'lang',
      alwaysRedirect: true,
      fallbackLocale: 'ja'
    },
    locales: [
      {
        name: 'English',
        code: 'en',
        iso: 'en-US',
        file: 'en.js'
      },
      {
        name: 'Japanese',
        code: 'ja',
        iso: 'ja-JP',
        file: 'ja.js'
      },
    ],
  },
  app: {
    head: {
      title: "Learn NuxtJs",
      meta: [
        {
          name: 'description',
          content: "Here is my content"
        }
      ]
    }
  },

})
