export const useAuth = () => {
  var isLogin = false;
  const setLogin = () => isLogin = true;

  return {
    isLogin, 
    setLogin
  }
}