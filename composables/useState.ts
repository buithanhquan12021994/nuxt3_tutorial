export const useCounter = (init: number) => useState<number>('counter', () => init);

export const useColor = (init: string) => useState<string>('color', () => init)