import { defineStore } from "pinia";

export const useRefCounterStore = defineStore('refcounter', () => {
  const refCount = ref(1);  
  const doubleCount = computed(() => refCount.value * 2);
  function increment() {
    refCount.value ++;
  }
  return { doubleCount, refCount, increment}
})