export default defineNuxtPlugin(() => {
  return {
    provide: {
      sayHelloPlugin: (msg) => `Hello from plugin ${msg}!`
    }
  }
})