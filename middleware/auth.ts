export default defineNuxtRouteMiddleware((to, from) => {
  const { isLogin } = useAuth();
  if (isLogin) {
    // redirect to page we want to go
    return;
  } 
  return navigateTo("/login")
})