let visit = 0;

export default async () => {
  await new Promise((resolve) => {
    setTimeout(() => {
      resolve(visit++);
    }, 2000);
  });

  return {
    data: visit
  }
}