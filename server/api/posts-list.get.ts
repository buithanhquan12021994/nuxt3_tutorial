import data from './posts.json';

export default defineEventHandler(() => {
  return {
    data
  }
});